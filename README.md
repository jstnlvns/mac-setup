# Mac Set-up and Tools

## Hardware
- 2017 27" iMac 5K
- Erogdox EZ
- PST Trackball

## Software
- [Productivity][1]
- [Database][2]
- [Files][13]

### Productivity
* [Finda][3]
* [MailMate][4] - I personally prefer an email client. Webmail is great but I tend to have a couple browsers going with multiple tabs and end up losing the mail tab.
* BusyCal
* [Dropshare][9] - Backblaze file sharing
* 1Password
* Brave
* Things
* Typinator - text expansion + text macro utility
* SnippetsLab
* [Script Debugger][15]
* Taskpaper
* Drafts for Mac

### Database
* [Postico][6]
* RazorSQL - Client for more of everything
* Tableplus - Client for everything

### Files
* Fetch
* Fork

### Terminal/Command-line
* Alacritty

### Text Editor/IDE
* BBEdit
* Neovim

### Documents/Writing
* Nisus Writer Pro

### Entertainment
* Swinsian - music player for FLAC and other music formats

[1]:#productivity
[2]:#database
[3]:https://keminglabs.com/finda/
[4]:https://freron.com/ "email client"
[5]:https://vivaldi.net/en-US/
[6]:https://eggerapps.at/postico/ "PostgreSQL DB browser and query tool"
[8]:https://contexts.co/ "Contexts app switcher"
[9]:https://getdropsha.re/ "Dropshare"
[10]:http://happenapps.com/ "Quiver"
[11]:https://iterm2.com/ "iTerm"
[12]:https://chocolatapp.com
[13]:#files
[14]:http://lightpaper.42squares.in
[15]:https://latenightsw.com/sd7/download/
